//
// by Joseph E Sloan joe.sloan@outlook.com
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

typedef enum {
	F_C  =   1,	// Carry 
	F_N  =   2, // Sub / Add 
	F_PV =   4, // Parity / Overflow 
	F_3  =   8, // Reserved 
	F_H  =  16, // Half carry 
	F_5  =  32, // Reserved
	F_Z  =  64, // Zero 
	F_S  = 128  // Sign 
} Z80Flags;

//
// Flags for enable / disable interrupts
//
static const int IE_DI = 0;
static const int IE_EI = 1;

//
// hardware mapped addresses
//
#define WATCHDOG 		0x50c0
#define INT_ENABLE 		0x5000
#define SOUND_ENABLE 	0x5001
#define FLIP_SCREEN		0x5003
#define INPUTS_0 		0x5000
#define INPUTS_1 		0x5040
#define DIP_1 			0x5080
#define DIP_2 			0x50c0

//
// memory boundaries
//
#define ROM_START 		0x0000
#define ROM_LENGTH		0x4000
#define RAM_START		0x4000
#define RAM_LENGTH		0x1000
#define SOUND_START 	0x5040
#define SOUND_LENGTH 	0x20
#define SPRITE_START 	0x5060
#define SPRITE_LENGTH 	0x10

typedef struct {
	uint8_t interruptEnabled;
	uint8_t soundEnabled;
	uint8_t screenFlipped;
	uint8_t playerOneStartLamp;
	uint8_t playerTwoStartLamp;
} STATUS;

typedef struct {	
	//
	// RAM/ROM
	//
	uint8_t 			rom[ROM_LENGTH];
	uint8_t				ram[RAM_LENGTH];
	uint8_t				soundData[SOUND_LENGTH];
	uint8_t				spriteData[SPRITE_LENGTH];
	uint16_t			palette[256];
	uint16_t			pc;
	//
	// registers
	//
	uint16_t			AF;
	uint16_t			BC;
	uint16_t			DE;
	uint16_t			HL;
	uint16_t			IX;
	uint16_t			IY;
	uint16_t			SP;

	uint8_t				A;
	uint8_t				B;
	uint8_t				C;
	uint8_t				D;
	uint8_t				E;
	uint8_t				H;
	uint8_t				L;

	uint16_t			sp; // stack pointer
	STATUS				state;
	uint8_t				I;
	uint8_t				IM;
	uint8_t				IFF1;
	uint8_t				IFF2;
	uint8_t				IE;
	// If true, defer checking maskable interrupts for one
	// instruction.  This is used to keep an interrupt from happening
	// immediately after an EI instruction. 
	uint8_t				 defer_int;
	//
	// flag
	//
	uint8_t				F;
} PACMAN_MACHINE;

static int parityBit[256] = { 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 
			1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1 };

static PACMAN_MACHINE* machine;
static void emulateZ80();

static PACMAN_MACHINE* initMemory() {

	PACMAN_MACHINE* pacm = calloc(1,sizeof(PACMAN_MACHINE));
	
	pacm->pc = 0;
	pacm->sp = 0x4fef;
	return pacm;
}

static void readFileIntoMemAt(unsigned char* buffer, char* filename, uint32_t offset, uint32_t size) {
	
	FILE *f= fopen(filename, "rb");
	if (f==NULL) {
		printf("error:	 Couldn't open %s\n", filename);
		exit(1);
	}
	fseek(f, 0L, SEEK_END);
	fseek(f, 0L, SEEK_SET);

	uint8_t* tmp = (buffer + offset); // point to offset location in buffer and fill
	fread(tmp, size, 1, f);
	fclose(f);
}

static void unimplementedInstruction(unsigned char* opcode) {
	
	printf ("Error:	 Unimplemented instruction\n");
	printf ("OPCODE: %02x\n",*opcode);
	printf("\n");
	free(machine);
	exit(1);
}

//
// Flag Operations
//
static void setFlag(Z80Flags flag) {
	
	machine->F |= flag;
}

static void resetFlag(Z80Flags flag) {
	
	machine->F &= ~flag;
}

static int getFlag(Z80Flags flag) {
	
	return (machine->F & flag) != 0;
}

static void valueFlag(Z80Flags flag, int value) {
	
	if(value) {
		setFlag(flag);
	} else {
		resetFlag(flag);
	}
}

static void setFlagsOther(uint8_t value) {
	
	valueFlag(F_5,(value & F_5) != 0);
	valueFlag(F_3,(value & F_3) != 0);
}

static void setFlagsZSP(uint8_t value) {
  	
	valueFlag(F_Z,(value == 0));
   	valueFlag(F_S,(0x80 == (value & 0x80)));
	valueFlag(F_PV,parityBit[value]);    
}

static void setFlagsLogic(int flagH) {
	
	valueFlag(F_S, (machine->A & 0x80) != 0);
	valueFlag(F_Z, (machine->A == 0));
	valueFlag(F_H, flagH);
	valueFlag(F_N, 0);
	valueFlag(F_C, 0);
	valueFlag(F_PV, parityBit[machine->A]);

	setFlagsOther(machine->A);
}

//
// mem read/write operations
//
static uint8_t read8(uint16_t addr) {

	//
	// READ FROM ROM
	//
	if(addr < RAM_START) {
		return machine->rom[addr];
	}

	//
	// READ FROM RAM
	//
	if(addr > RAM_START && addr < RAM_START + RAM_LENGTH) {
		
		addr -= RAM_START;

		return machine->ram[addr];
	}


}

static uint16_t read16(uint16_t addr) {
	
	uint8_t lsb = read8(addr);
	uint8_t msb = read8(addr + 1);
	return msb << 8 | lsb;
}

static void write8(uint16_t address, uint8_t value) {
	
	if(address < RAM_START) {
		printf("ERROR: write attempt to rom at %x\n",address);
		exit(1);
		return;
	}
	
	switch(address) {
		
		case WATCHDOG:
				break;
		case INT_ENABLE:
				break;
		case SOUND_ENABLE:
				break;
		case FLIP_SCREEN:
				break;

	}
	if(address >= 0x5000 && address <= 0x50ff) {
		
		printf("set mapped register %04x = %02x\n",address,value);

		return;
	}

	machine->ram[address] = value;
}

static void write16(uint16_t address, uint16_t value) {
	
	if(address < 0x4000) {
		printf("warning: write attempt to rom at %x\n",address);
		return;
	}
	
	write8(address, value);
	write8(address + 1, value >> 8);
}

static void writeRAMToFile() {
	
	FILE* file = fopen("memory.dump","wb");
	
	if(file == NULL) {
		printf("Error opening file\n");
		exit(1);
	}

	fwrite(&machine->ram,RAM_LENGTH,1,file);

	fclose(file);
}

static void writeROMToFile() {
	
	FILE* file = fopen("pacman.rom","wb");
	
	if(file == NULL) {
		printf("Error opening file\n");
		exit(1);
	}

	fwrite(&machine->rom,ROM_LENGTH,1,file);

	fclose(file);
}


//
// stack operations
//
static uint16_t pop() {
	
	uint16_t value = read16(machine->sp);
//	printf("pop %04x\n",value);
//	printf("at %04x\n",machine->sp);
	machine->sp++;
	machine->sp++;
	return value;
}

static void push(uint16_t value) {
	
//	printf("push: %04x\n",value);	
	machine->sp--;
	machine->sp--;
//	printf("at: %04x\n",machine->sp);
	write16(machine->sp,value);
}

//
// arithmitic
//
static uint8_t complement(uint8_t value) {
	
	if((value & 0x80) == 0) {
		return value;
	}

	value = ~value;
	value &= 0x7f;
	value++;

	return -value;
}

int main(int argc,char** argv) {
	
	machine = initMemory();

	unsigned char buffer[ 0x6000 ];

	readFileIntoMemAt(buffer,"pacman.6e",0,0x1000);
	readFileIntoMemAt(buffer,"pacman.6f",0x1000,0x1000);
	readFileIntoMemAt(buffer,"pacman.6h",0x2000,0x1000);
	readFileIntoMemAt(buffer,"pacman.6j",0x3000,0x1000);
	//readFileIntoMemAt(buffer,"pacman.5e",0x4000,0x1000);
	//readFileIntoMemAt(buffer,"pacman.5f",0x5000,0x1000);

	memcpy(machine->rom,buffer,ROM_LENGTH);

	//writeROMToFile();

	while(machine->pc < ROM_LENGTH) {
		emulateZ80();
	}
	
	printf("\n\n");

	free(machine);

	return 0;
}

static void disassembleZ80() {
	
	unsigned char* code = &machine->rom[machine->pc];
	int opBytes = 0;

	printf("%04x ", machine->pc);
	
	switch (*code) {
		case 0x00:	 printf("NOP"); break;
		case 0x01:	 printf("LD    BC,#$%02x%02x", code[2], code[1]); opBytes=3; break;
		case 0x02:	 printf("STAX   B"); break;
		case 0x03:	 printf("INC    BC"); break;
		case 0x04:	 printf("INC    B"); break;
		case 0x05:	 printf("DCR    B"); break;
		case 0x06:	 printf("LD    B,#$%02x", code[1]); opBytes=2; break;
		case 0x07:	 printf("RLCA"); break;
		case 0x08:	 printf("NOP"); break;
		case 0x09:	 printf("DAD    B"); break;
		case 0x0a:	 printf("LD   A,(BC)"); break;
		case 0x0b:	 printf("DCX    B"); break;
		case 0x0c:	 printf("INC    C"); break;
		case 0x0d:	 printf("DCR    C"); break;
		case 0x0e:	 printf("MVI    C,#$%02x", code[1]); opBytes = 2;	break;
		case 0x0f:	 printf("RRC"); break;
			
		case 0x10:	 printf("DJNZ	$%02x",code[1]); opBytes = 2; break;
		case 0x11:	 printf("LD    DE,#$%02x%02x", code[2], code[1]); opBytes=3; break;
		case 0x12:	 printf("LD     (DE),A"); break;
		case 0x13:	 printf("INC    DE"); break;
		case 0x14:	 printf("INR    D"); break;
		case 0x15:	 printf("DCR    D"); break;
		case 0x16:	 printf("LD    D,#$%02x", code[1]); opBytes=2; break;
		case 0x17:	 printf("RAL"); break;
		case 0x18:	 printf("NOP"); break;
		case 0x19:	 printf("ADD    HL,DE"); break;
		case 0x1a:	 printf("LD     A,(DE)"); break;
		case 0x1b:	 printf("DCX    D"); break;
		case 0x1c:	 printf("INR    E"); break;
		case 0x1d:	 printf("DCR    E"); break;
		case 0x1e:	 printf("MVI    E,#$%02x", code[1]); opBytes = 2; break;
		case 0x1f:	 printf("RAR"); break;
			
		case 0x20:	 printf("JR		NZ,$%02x",code[1]); opBytes = 2; break;
		case 0x21:	 printf("LD    HL,#$%02x%02x", code[2], code[1]); opBytes=3; break;
		case 0x22:	 printf("LD   $%02x%02x,HL", code[2], code[1]); opBytes=3; break;
		case 0x23:	 printf("INC    HL"); break;
		case 0x24:	 printf("INC    H"); break;
		case 0x25:	 printf("DCR    H"); break;
		case 0x26:	 printf("LD    H,#$%02x", code[1]); opBytes=2; break;
		case 0x27:	 printf("DAA"); break;
		case 0x28:	 printf("JR		Z,$%02x\n",code[1]); opBytes = 3; break;
		case 0x29:	 printf("DAD    H"); break;
		case 0x2a:	 printf("LD   HL,$%02x%02x", code[2], code[1]); opBytes=3; break;
		case 0x2b:	 printf("DEC    HL"); break;
		case 0x2c:	 printf("INC    L"); break;
		case 0x2d:	 printf("DCR    L"); break;
		case 0x2e:	 printf("MVI    L,#$%02x", code[1]); opBytes = 2; break;
		case 0x2f:	 printf("CMA"); break;
			
		case 0x30:	 printf("NOP"); break;
		case 0x31:	 printf("LXI    SP,#$%02x%02x", code[2], code[1]); opBytes=3; break;
		case 0x32:	 printf("LD    $%02x%02x,A", code[2], code[1]); opBytes=3; break;
		case 0x33:	 printf("INX    SP"); break;
		case 0x34:	 printf("INC    (HL)"); break;
		case 0x35:	 printf("DEC    (HL)"); break;
		case 0x36:	 printf("LD    (HL),#$%02x", code[1]); opBytes=2; break;
		case 0x37:	 printf("STC"); break;
		case 0x38:	 printf("NOP"); break;
		case 0x39:	 printf("DAD    SP"); break;
		case 0x3a:	 printf("LD    A,$%02x%02x", code[2], code[1]); opBytes=3; break;
		case 0x3b:	 printf("DCX    SP"); break;
		case 0x3c:	 printf("INC    A"); break;
		case 0x3d:	 printf("DCR    A"); break;
		case 0x3e:	 printf("LD    A,#$%02x", code[1]); opBytes = 2; break;
		case 0x3f:	 printf("CMC"); break;
			
		case 0x40:	 printf("MOV    B,B"); break;
		case 0x41:	 printf("MOV    B,C"); break;
		case 0x42:	 printf("MOV    B,D"); break;
		case 0x43:	 printf("MOV    B,E"); break;
		case 0x44:	 printf("MOV    B,H"); break;
		case 0x45:	 printf("MOV    B,L"); break;
		case 0x46:	 printf("MOV    B,M"); break;
		case 0x47:	 printf("LD    B,A"); break;
		case 0x48:	 printf("MOV    C,B"); break;
		case 0x49:	 printf("MOV    C,C"); break;
		case 0x4a:	 printf("MOV    C,D"); break;
		case 0x4b:	 printf("MOV    C,E"); break;
		case 0x4c:	 printf("MOV    C,H"); break;
		case 0x4d:	 printf("LD    C,L"); break;
		case 0x4e:	 printf("MOV    C,M"); break;
		case 0x4f:	 printf("LD    C,A"); break;
			
		case 0x50:	 printf("MOV    D,B"); break;
		case 0x51:	 printf("MOV    D,C"); break;
		case 0x52:	 printf("MOV    D,D"); break;
		case 0x53:	 printf("LD    D,E"); break;
		case 0x54:	 printf("MOV    D,H"); break;
		case 0x55:	 printf("MOV    D,L"); break;
		case 0x56:	 printf("LD     D,(HL)"); break;
		case 0x57:	 printf("MOV    D,A"); break;
		case 0x58:	 printf("MOV    E,B"); break;
		case 0x59:	 printf("MOV    E,C"); break;
		case 0x5a:	 printf("MOV    E,D"); break;
		case 0x5b:	 printf("MOV    E,E"); break;
		case 0x5c:	 printf("MOV    E,H"); break;
		case 0x5d:	 printf("MOV    E,L"); break;
		case 0x5e:	 printf("MOV    E,M"); break;
		case 0x5f:	 printf("LD    E,A"); break;

		case 0x60:	 printf("MOV    H,B"); break;
		case 0x61:	 printf("MOV    H,C"); break;
		case 0x62:	 printf("MOV    H,D"); break;
		case 0x63:	 printf("MOV    H.E"); break;
		case 0x64:	 printf("MOV    H,H"); break;
		case 0x65:	 printf("MOV    H,L"); break;
		case 0x66:	 printf("MOV    H,M"); break;
		case 0x67:	 printf("LD     H,A"); break;
		case 0x68:	 printf("MOV    L,B"); break;
		case 0x69:	 printf("MOV    L,C"); break;
		case 0x6a:	 printf("MOV    L,D"); break;
		case 0x6b:	 printf("MOV    L,E"); break;
		case 0x6c:	 printf("MOV    L,H"); break;
		case 0x6d:	 printf("MOV    L,L"); break;
		case 0x6e:	 printf("MOV    L,M"); break;
		case 0x6f:	 printf("LD     L,A"); break;

		case 0x70:	 printf("MOV    M,B"); break;
		case 0x71:	 printf("LD    (HL),C"); break;
		case 0x72:	 printf("LD    (HL),D"); break;
		case 0x73:	 printf("MOV    M.E"); break;
		case 0x74:	 printf("MOV    M,H"); break;
		case 0x75:	 printf("MOV    M,L"); break;
		case 0x76:	 printf("HLT");        break;
		case 0x77:	 printf("LD    (HL),A"); break;
		case 0x78:	 printf("MOV    A,B"); break;
		case 0x79:	 printf("MOV    A,C"); break;
		case 0x7a:	 printf("MOV    A,D"); break;
		case 0x7b:	 printf("MOV    A,E"); break;
		case 0x7c:	 printf("MOV    A,H"); break;
		case 0x7d:	 printf("MOV    A,L"); break;
		case 0x7e:	 printf("LD    A,(HL)"); break;
		case 0x7f:	 printf("MOV    A,A"); break;

		case 0x80:	 printf("ADD    B"); break;
		case 0x81:	 printf("ADD    C"); break;
		case 0x82:	 printf("ADD    D"); break;
		case 0x83:	 printf("ADD    E"); break;
		case 0x84:	 printf("ADD    H"); break;
		case 0x85:	 printf("ADD    A,L"); break;
		case 0x86:	 printf("ADD    A,(HL)"); break;
		case 0x87:	 printf("ADD    A,A"); break;
		case 0x88:	 printf("ADC    B"); break;
		case 0x89:	 printf("ADC    C"); break;
		case 0x8a:	 printf("ADC    D"); break;
		case 0x8b:	 printf("ADC    E"); break;
		case 0x8c:	 printf("ADC    A,H"); break;
		case 0x8d:	 printf("ADC    L"); break;
		case 0x8e:	 printf("ADC    M"); break;
		case 0x8f:	 printf("ADC    A"); break;

		case 0x90:	 printf("SUB    B"); break;
		case 0x91:	 printf("SUB    C"); break;
		case 0x92:	 printf("SUB    D"); break;
		case 0x93:	 printf("SUB    E"); break;
		case 0x94:	 printf("SUB    H"); break;
		case 0x95:	 printf("SUB    L"); break;
		case 0x96:	 printf("SUB    M"); break;
		case 0x97:	 printf("SUB    A"); break;
		case 0x98:	 printf("SBB    B"); break;
		case 0x99:	 printf("SBB    C"); break;
		case 0x9a:	 printf("SBB    D"); break;
		case 0x9b:	 printf("SBB    E"); break;
		case 0x9c:	 printf("SBB    H"); break;
		case 0x9d:	 printf("SBB    L"); break;
		case 0x9e:	 printf("SBB    M"); break;
		case 0x9f:	 printf("SBB    A"); break;

		case 0xa0:	 printf("ANA    B"); break;
		case 0xa1:	 printf("ANA    C"); break;
		case 0xa2:	 printf("ANA    D"); break;
		case 0xa3:	 printf("ANA    E"); break;
		case 0xa4:	 printf("ANA    H"); break;
		case 0xa5:	 printf("ANA    L"); break;
		case 0xa6:	 printf("ANA    M"); break;
		case 0xa7:	 printf("AND    A"); break;
		case 0xa8:	 printf("XRA    B"); break;
		case 0xa9:	 printf("XRA    C"); break;
		case 0xaa:	 printf("XRA    D"); break;
		case 0xab:	 printf("XRA    E"); break;
		case 0xac:	 printf("XRA    H"); break;
		case 0xad:	 printf("XRA    L"); break;
		case 0xae:	 printf("XRA    M"); break;
		case 0xaf:	 printf("XOR    A"); break;

		case 0xb0:	 printf("ORA    B"); break;
		case 0xb1:	 printf("ORA    C"); break;
		case 0xb2:	 printf("ORA    D"); break;
		case 0xb3:	 printf("ORA    E"); break;
		case 0xb4:	 printf("ORA    H"); break;
		case 0xb5:	 printf("ORA    L"); break;
		case 0xb6:	 printf("ORA    M"); break;
		case 0xb7:	 printf("ORA    A"); break;
		case 0xb8:	 printf("CMP    B"); break;
		case 0xb9:	 printf("CMP    C"); break;
		case 0xba:	 printf("CMP    D"); break;
		case 0xbb:	 printf("CMP    E"); break;
		case 0xbc:	 printf("CMP    H"); break;
		case 0xbd:	 printf("CMP    L"); break;
		case 0xbe:	 printf("CP    (HL)"); break;
		case 0xbf:	 printf("CMP    A"); break;

		case 0xc0:	 printf("RNZ"); break;
		case 0xc1:	 printf("POP    B"); break;
		case 0xc2:	 printf("JP     NZ,$%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xc3:	 printf("JP     $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xc4:	 printf("CALL   NZ,$%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xc5:	 printf("PUSH   B"); break;
		case 0xc6:	 printf("ADD    A,#$%02x",code[1]); opBytes = 2; break;
		case 0xc7:	 printf("RST    0"); break;
		case 0xc8:	 printf("RET 	Z"); break;
		case 0xc9:	 printf("RET"); break;
		case 0xca:	 printf("JP     Z,$%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xcb:	 printf("..");   break;
		case 0xcc:	 printf("CALL	Z,$%02x%02x",code[2],code[1]); opBytes = 3;   break;
		case 0xcd:	 printf("CALL   $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xce:	 printf("ACI    #$%02x",code[1]); opBytes = 2; break;
		case 0xcf:	 printf("RST    8"); break;

		case 0xd0:	 printf("RNC"); break;
		case 0xd1:	 printf("POP    DE"); break;
		case 0xd2:	 printf("JP     NC,$%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xd3:	 printf("OUT    #$%02x,A",code[1]); opBytes = 2; break;
		case 0xd4:	 printf("CNC    $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xd5:	 printf("PUSH   D"); break;
		case 0xd6:	 printf("SUI    #$%02x",code[1]); opBytes = 2; break;
		case 0xd7:	 printf("RST    10H"); break;
		case 0xd8:	 printf("RC");  break;
		case 0xd9:	 printf("RET"); break;
		case 0xda:	 printf("JC     $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xdb:	 printf("IN     #$%02x",code[1]); opBytes = 2; break;
		case 0xdc:	 printf("CC     $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xdd:	 printf(".."); opBytes = 3; break;
		case 0xde:	 printf("SBI    #$%02x",code[1]); opBytes = 2; break;
		case 0xdf:	 printf("RST    3"); break;

		case 0xe0:	 printf("RPO"); break;
		case 0xe1:	 printf("POP    HL"); break;
		case 0xe2:	 printf("JPO    $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xe3:	 printf("XTHL");break;
		case 0xe4:	 printf("CPO    $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xe5:	 printf("PUSH   HL"); break;
		case 0xe6:	 printf("AND    #$%02x",code[1]); opBytes = 2; break;
		case 0xe7:	 printf("RST    20H"); break;
		case 0xe8:	 printf("RPE"); break;
		case 0xe9:	 printf("JP		(HL)");break;
		case 0xea:	 printf("JPE    $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xeb:	 printf("EX		DE,HL"); break;
		case 0xec:	 printf("CPE     $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xed:	 printf(".."); opBytes = 3; break;
		case 0xee:	 printf("XRI    #$%02x",code[1]); opBytes = 2; break;
		case 0xef:	 printf("RST    5"); break;

		case 0xf0:	 printf("RP");  break;
		case 0xf1:	 printf("POP    AF"); break;
		case 0xf2:	 printf("JP     $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xf3:	 printf("DI");  break;
		case 0xf4:	 printf("CP     $%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xf5:	 printf("PUSH   AF"); break;
		case 0xf6:	 printf("ORI    #$%02x",code[1]); opBytes = 2; break;
		case 0xf7:	 printf("RST    6"); break;
		case 0xf8:	 printf("RM");  break;
		case 0xf9:	 printf("SPHL");break;
		case 0xfa:	 printf("JP     M,$%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xfb:	 printf("EI");  break;
		case 0xfc:	 printf("CALL   M,$%02x%02x",code[2],code[1]); opBytes = 3; break;
		case 0xfd:	 printf(".."); break;
		case 0xfe:	 printf("CP    #$%02x",code[1]); opBytes = 2; break;
		case 0xff:	 printf("RST    7"); break;
	}

	printf("\n");

	//machine->pc += opBytes;
}
static void  emulateZ80() {
	
	uint8_t* code = &machine->rom[machine->pc];

	disassembleZ80();

	switch(*code)													
	{	
		case 0x00:	 										// NOP
				machine->pc++;
				break; 
		case 0x01:											// LD BC,word
				machine->C = code[1];
				machine->B = code[2];
				machine->pc += 3;
			break;
		case 0x02:	unimplementedInstruction(code); break; 
		case 0x03:											// INC BC
					{
						uint16_t value = (machine->B << 8) | (machine->C);
						value++;
						machine->B = (value & 0xff00) >> 8;
						machine->C = (value & 0xff);
						machine->pc++;
					}
				break;
		case 0x04: {										// INC B
						valueFlag(F_PV, !(machine->B & 0x80) && ((machine->B + 1) & 0x80));
						machine->B++;
						valueFlag(F_H, !(machine->B & 0x0F));
						valueFlag(F_S, ((machine->B & 0x80) != 0));
						valueFlag(F_Z, (machine->B == 0));
						valueFlag(F_N, 0);
						setFlagsOther(machine->B);
						machine->pc++;
				   }
				break; 
		case 0x05:	unimplementedInstruction(code); break; 
		case 0x06:											// LD B,byte
					{
						machine->B = code[1];
						machine->pc += 2;
					}
				break;
		case 0x07:											// RLCA
			   		{
						uint8_t value = machine->A;	
						valueFlag(F_C, (value & 0x80) != 0);
						value <<= 1;
						value |= getFlag(F_C);
						setFlagsOther(value);
						resetFlag(F_H | F_N);
						machine->A = value;
						machine->pc++;
					}
				break;	
		case 0x08:	unimplementedInstruction(code); break;
		case 0x09:	unimplementedInstruction(code); break;
		case 0x0a:											// LD A,(BC)	
					{
						uint16_t offset = (machine->B << 8) | (machine->C);
						machine->A = read8(offset);
						machine->pc++;
					}	
				break;
		case 0x0b:	unimplementedInstruction(code); break; 
		case 0x0c:											// INC C
					{
						uint8_t value = machine->C;
						valueFlag(F_PV, !(value & 0x80) && ((value + 1) & 0x80));
						value++;
						valueFlag(F_H, !(value & 0x0F));
						valueFlag(F_S, ((value & 0x80) != 0));
						valueFlag(F_Z, (value == 0));
						valueFlag(F_N, 0);
						setFlagsOther(value);

						machine->C = value;

						machine->pc++;
					}
				break;
		case 0x0d:	unimplementedInstruction(code); break;
		case 0x0e:	unimplementedInstruction(code); break;
		case 0x0f:	unimplementedInstruction(code); break; 
		case 0x10:											// DJNZ index
					{
						machine->B--;
						if(machine->B) {
							char index = read8(machine->pc+1);
							machine->pc += 2;
							machine->pc += index;
						} else {
							machine->pc += 2;
						}
					}
				break;
		case 0x11: 											// LD DE,word
					{
						machine->E = code[1];
						machine->D = code[2];
						machine->pc += 3;
					}
				break;
		case 0x12:											// LD (DE),A
					{
						uint16_t de = (machine->D << 8) | machine->E;
						write8(de,machine->A);
						machine->pc++;
					}
				break;
		case 0x13:											// INC DE
					{
						uint16_t de = (machine->D << 8) | machine->E;
						de++;
						
						machine->D = (de & 0xff00) >> 8;
						machine->E = (de & 0xff);

						machine->pc++;

					}
				break;
		case 0x14:	unimplementedInstruction(code); break;
		case 0x15:	unimplementedInstruction(code); break;
		case 0x16:											// LD D,byte
					{
						machine->D = code[1];
						machine->pc += 2;
					}
				break;
		case 0x17:	unimplementedInstruction(code); break; 
		case 0x18:	unimplementedInstruction(code); break;
		case 0x19: 											 // ADD HL,DE
					{
						uint16_t hl = (machine->H << 8) | machine->L;
			   			uint16_t de = (machine->D << 8) | machine->E;

						valueFlag(F_H, ((hl & 0x0fff) + (de & 0x0fff)) & 0x1000);

						uint32_t value = hl + de;
						
						valueFlag(F_C, value & 0x10000);
						valueFlag(F_N, 0);

						setFlagsOther(value >> 8);

						machine->H = (value & 0xff00) >> 8;
						machine->L = value & 0xff;
					
						machine->pc++;	
					}
				break;
		case 0x1a:											// LD A,(DE)
					{
						uint16_t de = (machine->D << 8) | machine->E;
						machine->A = read8(de);
						machine->pc++;
					}
				break;
		case 0x1b:	unimplementedInstruction(code); break; 
		case 0x1c:	unimplementedInstruction(code); break;
		case 0x1d:	unimplementedInstruction(code); break;
		case 0x1e:	unimplementedInstruction(code); break;
		case 0x1f:	unimplementedInstruction(code); break; 
		case 0x20:											// JR NZ,index
					{
						char index = complement(read8(machine->pc + 1));

						if((getFlag(F_Z) == 0)) {
							machine->pc++;
							machine->pc++;
							printf("index %d\n",index);
							machine->pc += index;
						} else {
							machine->pc += 2;
						}
					}
				break;
		case 0x21:											// LD HL,word
					{
						machine->L = code[1];
						machine->H = code[2];
						machine->pc += 3;
					}
				break;
		case 0x22:											// LD (word),HL
					{
						uint16_t value = (machine->H << 8) | (machine->L);
						//uint16_t offset = (code[2] << 8) | (code[1]);
						uint16_t offset = read16(machine->pc + 1);
						write8(offset,value);
						machine->pc += 3;
					}
				break;
		case 0x23:											// INC HL 
					{
						uint16_t value = (machine->H << 8) | (machine->L);
						value++;
						machine->H = (value & 0xff00) >> 8;
						machine->L = value & 0xff;
						machine->pc++;
					}
				break;
		case 0x24:											// INC H
					{
						uint8_t value = machine->H;
						valueFlag(F_PV, !(value & 0x80) && ((value + 1) & 0x80));
						value++;
						valueFlag(F_H, !(value & 0x0f) == 0x0f);
						valueFlag(F_S, ((value & 0x80) != 0));
					    valueFlag(F_Z, (value == 0));
					    valueFlag(F_N, 0);
						setFlagsOther(value);
						machine->H = value;
						machine->pc++;
					}
				break;
		case 0x25:	unimplementedInstruction(code); break;
		case 0x26:											// LD H,byte
					{
						machine->H = code[1];
						machine->pc += 2;
					}
				break;
		case 0x27:											// DAA
					{
						int correction_factor = 0x00;
						int carry = 0;
						
						if(machine->A > 0x99 || getFlag(F_C)) {
							correction_factor |= 0x60;
							carry = 1;
						}
						
						if((machine->A & 0x0f) > 9 || getFlag(F_H)) {
							correction_factor |= 0x06;
						}
						
						int a_before = machine->A;
						
						if(getFlag(F_N)) {
							machine->A -= correction_factor;
						} else {              
							machine->A += correction_factor;
						}
						
						valueFlag(F_H, (a_before ^ machine->A) & 0x10);
						valueFlag(F_C, carry);
						valueFlag(F_S, (machine->A & 0x80) != 0);
						valueFlag(F_Z, (machine->A == 0));
						valueFlag(F_PV, parityBit[machine->A]);
						
						setFlagsOther(machine->A);

						machine->pc++;

					}
				break;
		case 0x28:											// JR Z,index
					{
						char index = complement(read8(machine->pc + 1));

						if(getFlag(F_Z)) {
							machine->pc++;
							machine->pc++;
							machine->pc += index;
						} else {
							machine->pc += 2;
						}
					}
				break;
		case 0x29:	unimplementedInstruction(code); break;
		case 0x2a:											// LD HL,(word)
					{
						uint16_t value = read16(machine->pc + 1);
						
						machine->H = (value & 0xff00) >> 8;
						machine->L = value & 0xff;
						
						machine->pc += 3;
					}
				break;
		case 0x2b:											// DEC HL
					{
						uint16_t value = (machine->H << 8) | (machine->L);
						value--;
						machine->H = (value & 0xff00) >> 8;
						machine->L = value & 0xff;
						machine->pc++;
					}
				break;
		case 0x2c:											// INC L
					{
						valueFlag(F_PV, !(machine->L & 0x80) && ((machine->L + 1) & 0x80));
						machine->L++;
						valueFlag(F_H, !(machine->L & 0x0F));
						valueFlag(F_S, ((machine->L & 0x80) != 0));
						valueFlag(F_Z, (machine->L == 0));
						valueFlag(F_N, 0);
						setFlagsOther(machine->L);
						machine->pc++;
					}
				break;
		case 0x2d:	unimplementedInstruction(code); break;
		case 0x2e:	unimplementedInstruction(code); break;
		case 0x2f:	unimplementedInstruction(code); break;
		case 0x30:	unimplementedInstruction(code); break;
		case 0x31:	unimplementedInstruction(code); break;
		case 0x32:											// LD word,A
					{
						uint16_t offset = read16(machine->pc + 1);
						write8(offset,machine->A);
						printf("offset: $%04x\n",offset);
						machine->pc += 3;
					}
				break;
		case 0x33:	unimplementedInstruction(code); break;
		case 0x34:											// INC (HL)
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						uint8_t value = read8(offset);

						valueFlag(F_PV, !(value & 0x80) && ((value + 1) & 0x80));
						value++;
						valueFlag(F_H, !(value & 0x0F));
						valueFlag(F_S, ((value & 0x80) != 0));
						valueFlag(F_Z, (value == 0));
						valueFlag(F_N, 0);
						setFlagsOther(value);

						write8(offset,value);
						machine->pc++;
					}	
			break;
		case 0x35:											// DEC (HL)
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						uint8_t value = read8(offset);

						valueFlag(F_PV, (value & 0x80) && ((value + 1) & 0x80));
						value--;
						valueFlag(F_H, (value & 0x0F));
						valueFlag(F_S, ((value & 0x80) != 0));
						valueFlag(F_Z, (value == 0));
						valueFlag(F_N, 1);
						setFlagsOther(value);

						write8(offset,value);
						machine->pc++;

					}	
				break;
		case 0x36:											// LD (HL),byte
				  	{
						uint8_t value = read8(machine->pc + 1);
						uint16_t offset = (machine->H << 8) | (machine->L);

						write8(offset,value);

						machine->pc += 2;
					}
				break;
		case 0x37:	unimplementedInstruction(code); break;
		case 0x38:	unimplementedInstruction(code); break;
		case 0x39:	unimplementedInstruction(code); break;
		case 0x3a:											// LD A,word
					{
						uint16_t offset = (code[1] << 8) | (code[2]);
						machine->A = read8(offset);
						machine->pc += 3;	
					}
				break;
		case 0x3b:	unimplementedInstruction(code); break;
		case 0x3c:											// INC A
					{
						uint8_t value = machine->A;
						valueFlag(F_PV, !(value & 0x80) && ((value + 1) & 0x80));
						value++;
						valueFlag(F_H, !(value & 0x0f));
						valueFlag(F_S, ((value & 0x80) != 0));
						valueFlag(F_Z, (value == 0));
						valueFlag(F_N, 0);
						setFlagsOther(value);
						machine->A = value;
						machine->pc++;
					}
				break;
		case 0x3d:	unimplementedInstruction(code); break;
		case 0x3e:											// LD A,byte
					{
						machine->A = read8(machine->pc + 1);
						machine->pc += 2;
					}
				break;
		case 0x3f:	unimplementedInstruction(code); break;
		case 0x40:	unimplementedInstruction(code); break;
		case 0x41:	unimplementedInstruction(code); break;
		case 0x42:	unimplementedInstruction(code); break;
		case 0x43:	unimplementedInstruction(code); break;
		case 0x44:	unimplementedInstruction(code); break;
		case 0x45:	unimplementedInstruction(code); break;
		case 0x46:	unimplementedInstruction(code); break;
		case 0x47:											// LD B,A
					{
						machine->B = machine->A;
						machine->pc++;
					}
				break;
		case 0x48:	unimplementedInstruction(code); break;
		case 0x49:	unimplementedInstruction(code); break;
		case 0x4a:	unimplementedInstruction(code); break;
		case 0x4b:	unimplementedInstruction(code); break;
		case 0x4c:	unimplementedInstruction(code); break;
		case 0x4d:											// LD C,L
					{
						machine->C = machine->L;
						machine->pc++;
					}
				break;
		case 0x4e:	unimplementedInstruction(code); break;
		case 0x4f:											// LD C,A
					{
						machine->C = machine->A;
						machine->pc++;
					}
				break;
		case 0x50:	unimplementedInstruction(code); break;
		case 0x51:	unimplementedInstruction(code); break;
		case 0x52:	unimplementedInstruction(code); break;
		case 0x53:											// LD D,E
					{
						machine->D = machine->E;
						machine->pc++;
					}
				break;
		case 0x54:	unimplementedInstruction(code); break;
		case 0x55:	unimplementedInstruction(code); break;
		case 0x56:											// LD D,(HL)
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						uint8_t value = read8(offset);
						
						machine->D = value;
						machine->pc++;
					}
				break;
		case 0x57:	unimplementedInstruction(code); break;
		case 0x58:	unimplementedInstruction(code); break;
		case 0x59:	unimplementedInstruction(code); break;
		case 0x5a:	unimplementedInstruction(code); break;
		case 0x5b:	unimplementedInstruction(code); break;
		case 0x5c:	unimplementedInstruction(code); break;
		case 0x5d:	unimplementedInstruction(code); break;
		case 0x5e:  unimplementedInstruction(code); break;										//
		case 0x5f:											// LD E,A
					{
						machine->E = machine->A;
						machine->pc++;
					}
				break;
		case 0x60:	unimplementedInstruction(code); break;
		case 0x61:	unimplementedInstruction(code); break;
		case 0x62:	unimplementedInstruction(code); break;
		case 0x63:	unimplementedInstruction(code); break;
		case 0x64:	unimplementedInstruction(code); break;
		case 0x65:	unimplementedInstruction(code); break;
		case 0x66:	unimplementedInstruction(code); break;
		case 0x67:											// LD H,A
					{
						machine->H = machine->A;
						machine->pc++;
					}
				break;
		case 0x68:	unimplementedInstruction(code); break;
		case 0x69:	unimplementedInstruction(code); break;
		case 0x6a:	unimplementedInstruction(code); break;
		case 0x6b:	unimplementedInstruction(code); break;
		case 0x6c:	unimplementedInstruction(code); break;
		case 0x6d:	unimplementedInstruction(code); break;
		case 0x6e:	unimplementedInstruction(code); break;
		case 0x6f:											// LD L,A
					{
						machine->L = machine->A;
						machine->pc++;
					}
				break;
		case 0x70:	unimplementedInstruction(code); break;
		case 0x71:											// LD (HL),C
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						write8(offset,machine->C);
						machine->pc++;
					}
				break;
		case 0x72:											// LD (HL),D
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						write8(offset,machine->D);
						machine->pc++;
					}
				break;
		case 0x73:	unimplementedInstruction(code); break;
		case 0x74:	unimplementedInstruction(code); break;
		case 0x75:	unimplementedInstruction(code); break;
		case 0x76:	unimplementedInstruction(code); break;
		case 0x77: {										// LD (HL),A
						uint16_t offset = (machine->H << 8) | (machine->L);
						write8(offset,machine->A);
						machine->pc++;
				   }
				break;
		case 0x78:	unimplementedInstruction(code); break;
		case 0x79:	unimplementedInstruction(code); break;
		case 0x7a:	unimplementedInstruction(code); break;
		case 0x7b:	unimplementedInstruction(code); break;
		case 0x7c:	unimplementedInstruction(code); break;
		case 0x7d:	unimplementedInstruction(code); break;
		case 0x7e:											// LD A,(HL)
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						machine->A = read8(offset);
						machine->pc++;
					}
				break;
		case 0x7f:	unimplementedInstruction(code); break;
		case 0x80:	unimplementedInstruction(code); break;
		case 0x81:	unimplementedInstruction(code); break;
		case 0x82:	unimplementedInstruction(code); break;
		case 0x83:	unimplementedInstruction(code); break;
		case 0x84:	unimplementedInstruction(code); break;
		case 0x85:											// ADD A,L
					{
						resetFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0F) + (machine->L & 0x0F)) & 0x10) != 0);

						uint16_t result = machine->A + machine->L;

						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));

						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = machine->L & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign == subtrahend_sign && result_sign != minuend_sign;
					
						valueFlag(F_PV, overflow);
						setFlagsOther(result);
					
						machine->A = (result & 0xff);
						machine->pc++;	

					}
				break;
		case 0x86:											// ADD A,(HL)
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						uint8_t value = read8(offset);
						
						resetFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0F) + (value & 0x0F)) & 0x10) != 0);

						uint16_t result = machine->A + value;

						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));

						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = value & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign == subtrahend_sign && result_sign != minuend_sign;
					
						valueFlag(F_PV, overflow);
						setFlagsOther(result);
					
						machine->A = (result & 0xff);
						machine->pc++;	

					}
				break;
		case 0x87:											// ADD A,A
					{
						resetFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0F) + (machine->A & 0x0F)) & 0x10) != 0);
						
						uint16_t result = machine->A + machine->A;
						
						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));
						
						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = machine->A & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign == subtrahend_sign && result_sign != minuend_sign;
						
						valueFlag(F_PV, overflow);
						setFlagsOther(result);
						
						machine->A = (result & 0xff);
						machine->pc++;	
					}
				break;
		case 0x88:	unimplementedInstruction(code); break;
		case 0x89:	unimplementedInstruction(code); break;
		case 0x8a:	unimplementedInstruction(code); break;
		case 0x8b:	unimplementedInstruction(code); break;
		case 0x8c:											// ADC A,H
					{
						//
						// carry 1
						//
						resetFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0F) + (machine->H & 0x0F)) & 0x10) != 0);

						uint16_t result = machine->A + machine->H;

						if(getFlag(F_C)) {
							result++;
						}
						
						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));
						
						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = machine->H & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign == subtrahend_sign && result_sign != minuend_sign;
						
						valueFlag(F_PV, overflow);
						setFlagsOther(result);
						
						machine->A = (result & 0xff);
						machine->pc++;	

					}
				break;
		case 0x8d:	unimplementedInstruction(code); break;
		case 0x8e:	unimplementedInstruction(code); break;
		case 0x8f:	unimplementedInstruction(code); break;
		case 0x90:	unimplementedInstruction(code); break;
		case 0x91:	unimplementedInstruction(code); break;
		case 0x92:	unimplementedInstruction(code); break;
		case 0x93:	unimplementedInstruction(code); break;
		case 0x94:	unimplementedInstruction(code); break;
		case 0x95:	unimplementedInstruction(code); break;
		case 0x96:	unimplementedInstruction(code); break;
		case 0x97:	unimplementedInstruction(code); break;
		case 0x98:	unimplementedInstruction(code); break;
		case 0x99:	unimplementedInstruction(code); break;
		case 0x9a:	unimplementedInstruction(code); break;
		case 0x9b:	unimplementedInstruction(code); break;
		case 0x9c:	unimplementedInstruction(code); break;
		case 0x9d:	unimplementedInstruction(code); break;
		case 0x9e:	unimplementedInstruction(code); break;
		case 0x9f:	unimplementedInstruction(code); break;
		case 0xa0:	unimplementedInstruction(code); break;
		case 0xa1:	unimplementedInstruction(code); break;
		case 0xa2:	unimplementedInstruction(code); break;
		case 0xa3:	unimplementedInstruction(code); break;
		case 0xa4:	unimplementedInstruction(code); break;
		case 0xa5:	unimplementedInstruction(code); break;
		case 0xa6:	unimplementedInstruction(code); break;
		case 0xa7:											// AND A
					{
						machine->A &= machine->A;
						setFlagsLogic(1);
						machine->pc++;
					}
			break;
		case 0xa8:	unimplementedInstruction(code); break;
		case 0xa9:	unimplementedInstruction(code); break;
		case 0xaa:	unimplementedInstruction(code); break;
		case 0xab:	unimplementedInstruction(code); break;
		case 0xac:	unimplementedInstruction(code); break;
		case 0xad:	unimplementedInstruction(code); break;
		case 0xae:	unimplementedInstruction(code); break;
		case 0xaf:											// XOR A
					{
						machine->A ^= machine->A;
						setFlagsLogic(0);
						machine->pc++;
					}	
			break;
		case 0xb0:	unimplementedInstruction(code); break;
		case 0xb1:	unimplementedInstruction(code); break;
		case 0xb2:	unimplementedInstruction(code); break;
		case 0xb3:	unimplementedInstruction(code); break;
		case 0xb4:	unimplementedInstruction(code); break;
		case 0xb5:	unimplementedInstruction(code); break;
		case 0xb6:	unimplementedInstruction(code); break;
		case 0xb7:	unimplementedInstruction(code); break;
		case 0xb8:	unimplementedInstruction(code); break;
		case 0xb9:	unimplementedInstruction(code); break;
		case 0xba:	unimplementedInstruction(code); break;
		case 0xbb:	unimplementedInstruction(code); break;
		case 0xbc:	unimplementedInstruction(code); break;
		case 0xbd:	unimplementedInstruction(code); break;
		case 0xbe:											// CP (HL)
					{
						uint16_t offset = (machine->H << 8) | (machine->L);
						uint8_t value = read8(offset);

						setFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0F) - (value & 0x0F)) & 0x10) != 0);
						
						uint16_t result = machine->A - value;
						
						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));

						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = value & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign != subtrahend_sign && result_sign != minuend_sign;

						valueFlag(F_PV, overflow);
						setFlagsOther(result);

						machine->pc++;
					}
				break;
		case 0xbf:	unimplementedInstruction(code); break;
		case 0xc0:	unimplementedInstruction(code); break;
		case 0xc1:	unimplementedInstruction(code); break;
		case 0xc2:											// JP NZ,address
					{
						uint16_t offset = read16(machine->pc + 1);
						
						if((getFlag(F_Z) == 0)) {
							machine->pc = offset;
						} else {
							machine->pc += 3;
						}
					}
				break;
		case 0xc3:											// JP address
					{
						machine->pc = (code[2] << 8 | code[1]);	
					}
				break; 
		case 0xc4:											// CALL NZ,address
					{
						uint16_t address = read16(machine->pc + 1);

						machine->pc += 3;

						if((getFlag(F_Z) == 0)) {
							push(machine->pc);
							machine->pc = address;
						}
					}
				break;
		case 0xc5:	unimplementedInstruction(code); break;
		case 0xc6:											// ADD A,byte
					{
						uint8_t value = read8(machine->pc + 1);
						
						resetFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0F) + (value & 0x0F)) & 0x10) != 0);
						
						uint16_t result = machine->A + value;
						
						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));

						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = value & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign == subtrahend_sign && result_sign != minuend_sign;

						valueFlag(F_PV, overflow);
						setFlagsOther(result);

						machine->A = (result & 0xff);

						machine->pc += 2;
					}
				break;
		case 0xc7:	unimplementedInstruction(code); break;
		case 0xc8:											// RET Z
					{
						if(getFlag(F_Z)) {
							machine->pc = pop();
							//machine->pc = machine->ram[machine->sp] | (machine->ram[machine->sp+1] << 8);
						} else {
							machine->pc++;
						}
					}
			break;
		case 0xc9:											// RET
					{
						machine->pc = pop();
					//	printf("pc: %04x\n",machine->pc);
					//	writeMemToFile();
					//	exit(0);
					}
				break;
		case 0xca:											// JP Z,address
					{
						if(getFlag(F_Z)) {
							machine->pc = (code[2] << 8 | code[1]);	 
						} else {
							machine->pc += 3;
						}
					}
			break;
		case 0xcb:	
					{
						if(code[1] == 0xce) {				// SET 1,(HL)
							
							printf("SET 1,(HL)\n");

							uint16_t offset = (machine->H << 8) | (machine->L);
							uint8_t value = read8(offset);
							
							value |= (1 << 1);
							write8(offset,value);
							machine->pc += 2;
						
						} else if(code[1] == 0x67) {		// BIT 4,A
							
							printf("BIT 4,A\n");
							if(machine->A & (1 << 4)) {
									resetFlag(F_Z | F_PV);
							} else {
									setFlag(F_H);
							}

							setFlag(F_H);

							resetFlag(F_N);
							resetFlag(F_S);

							valueFlag(F_5, machine->A & F_5);
							valueFlag(F_3, machine->A & F_3);

							machine->pc += 2;

						} else {
							printf("16 bit opcode: %02x%02x\n",read8(machine->pc),read8(machine->pc + 1));
							exit(0);
						}
					}
				break;
		case 0xcc:											// CALL Z,address
					{
						uint16_t address = read16(machine->pc + 1);
						
						machine->pc += 3;

						if(getFlag(F_Z)) {
							push(machine->pc);
							machine->pc = address;	
						}
					}
				break;
		case 0xcd:											// CALL address
					{
						uint16_t address = read16(machine->pc + 1);
						machine->pc += 3;
						push(machine->pc);
						machine->pc = address;
					}
				break;
		case 0xce:	unimplementedInstruction(code); break;
		case 0xcf:											// RST 8
					{
						push(machine->pc);
						printf("pushing value: %04x\n",machine->pc);
						//writeMemToFile();
						//exit(0);
						machine->pc = 0x08;
					}	
				break;
		case 0xd0:	unimplementedInstruction(code); break;
		case 0xd1:											// POP DE
					{
						uint16_t value = pop();
						machine->D = (value & 0xff00) >> 8;
						machine->E = value & 0xff;
						machine->pc++;
					}
				break;
		case 0xd2:											// JP NC,address
					{
						uint16_t address = read16(machine->pc + 1);

						if((getFlag(F_C) == 0)) {
							machine->pc = address;
						} else {
							machine->pc += 3;
						}

					}
				break;
		case 0xd3:											// OUT (byte),A
					{
						exit(1);
					}
				break;
		case 0xd4:	unimplementedInstruction(code); break;
		case 0xd5:	unimplementedInstruction(code); break;
		case 0xd6:	unimplementedInstruction(code); break;
		case 0xd7:											// RST 10H
					{
						machine->pc++;

						push(machine->pc);

						machine->pc = 0x010;
					}
				break;
		case 0xd8:	unimplementedInstruction(code); break;
		case 0xd9:	unimplementedInstruction(code); break;
		case 0xda:	unimplementedInstruction(code); break;
		case 0xdb:	unimplementedInstruction(code); break;
		case 0xdc:	unimplementedInstruction(code); break;
		case 0xdd:	
					{
						if(code[1] == 0x77)	{				// LD (IX+index),A
							
							printf("LD\t(IX+index),A\n");
							
							uint8_t offset = read8(machine->pc + 2);
							write8(machine->IX + offset,machine->A);
							machine->pc += 3;	
						
						} else if (code[1] == 0x7e) {		// LD A,(IX+index)
							
							printf("LD\tA.(IX+index)\n");
							
							uint8_t offset = read8(machine->pc + 2);
							machine->A = read8(machine->IX + offset);
							machine->pc += 3;
						
						} else if (code[1] == 0x21) {		// LD IX,word
							
							printf("LD\tIX,$%02x%02x\n",code[3],code[2]);
							
							uint16_t value = read16(machine->pc+2);
							machine->IX = value;
							machine->pc += 4;
						
						} else if(code[1] == 0x19) {		// ADD IX,DE
							
							printf("ADD\tIX,DE\n");
							
							uint16_t ix = machine->IX;
							uint16_t de = (machine->D << 8) | (machine->E);
							uint16_t sum = ix + de;
							
							valueFlag(F_H, ((ix & 0x0fff) + (de & 0x0fff)) & 0x1000);
							valueFlag(F_C, sum & 0x10000);
							valueFlag(F_N, 0);
							setFlagsOther(sum >> 8);
							machine->IX = sum;
							machine->pc += 2;
						} else if(code[1] == 0x75) {		// LD (IX+index),L
							
							printf("LD\t(IX+index),L\n");
							
							uint8_t offset = read8(machine->pc + 2);
							write8(machine->IX + offset,machine->L);
							machine->pc += 3;	

						} else if(code[1] == 0x74) {		// LD (IX+index),H
							
							printf("LD\t(IX+index),H\n");
							
							uint8_t offset = read8(machine->pc + 2);
							write8(machine->IX + offset,machine->H);
							machine->pc += 3;

						} else if(code[1] == 0x73) { 		// LD (IX+index),E
						
							printf("LD\t(IX+index),E\n");
							
							uint8_t offset = read8(machine->pc + 2);
							write8(machine->IX + offset,machine->E);
							machine->pc += 3;	

						} else if(code[1] == 0x72) { 		// LD (IX+index),D
							
							printf("LD\t(IX+index),D\n");
							
							uint8_t offset = read8(machine->pc + 2);
							write8(machine->IX + offset,machine->D);
							machine->pc += 3;	

						} else {
							printf("16-bit opcode DD%02x\n",code[1]);
							exit(0);
						}
							
					}
				break;
		case 0xde:	unimplementedInstruction(code); break;
		case 0xdf:	unimplementedInstruction(code); break;
		case 0xe0:	unimplementedInstruction(code); break;
		case 0xe1:											// POP HL
					{
						uint16_t value = pop();
						machine->H = (value & 0xff00) >> 8;
						machine->L = value & 0xff;
						machine->pc++;
	
					}
				break;
		case 0xe2:	unimplementedInstruction(code); break;
		case 0xe3:	unimplementedInstruction(code); break;
		case 0xe4:	unimplementedInstruction(code); break;
		case 0xe5:											// PUSH HL
					{
						push((machine->H << 8) | (machine->L));
						machine->pc++;
					}
				break;
		case 0xe6:											// AND byte
					{
						uint8_t value = read8(machine->pc + 1);
						machine->A &= value;
						setFlagsLogic(1);
						machine->pc += 2;
					}
				break;
		case 0xe7:											// RST 20H
					{
						machine->pc++;

						push(machine->pc);

						machine->pc = 0x020;
					}
				break;
		case 0xe8:	unimplementedInstruction(code); break;
		case 0xe9:											// JP (HL)
					{
						uint16_t offset = (machine->H << 8) | machine->L;
						//uint16_t value = read8(offset);
						//free(machine);
						//exit(0);
						machine->pc = offset;
					}
				break;
		case 0xea:	unimplementedInstruction(code); break;
		case 0xeb:											// EX DE,HL
					{
						uint16_t hl = (machine->H << 8) | machine->L;
			   			uint16_t de = (machine->D << 8) | machine->E;
						uint16_t temp = de;
						
						de = hl;
						hl = temp;

						machine->D = (de & 0xff00) >> 8;
						machine->E = de & 0xff;
							
						machine->H = (hl & 0xff00) >> 8;
						machine->L = hl & 0xff;

						machine->pc++;

					}
				break;
		case 0xec:	unimplementedInstruction(code); break;
		case 0xed:	
					{
						if(code[1] == 0x52) {				// SBC HL,DE
							
							printf("SBC\tHL,DE\n");
							
							uint16_t hl = (machine->H << 8) | machine->L;
			   				uint16_t de = (machine->D << 8) | machine->E;

							if(getFlag(F_C)) {
								de++;
							}
							
							uint16_t result = hl;
							result -= de;

							valueFlag(F_H, ((hl & 0x0fff) - (de & 0x0fff)) & 0x1000);
							valueFlag(F_C, result & 0x10000);
							
							int minuend_sign = hl & 0x8000;
							int subtrahend_sign = de & 0x8000;
							int result_sign = result & 0x8000;
							int overflow = minuend_sign != subtrahend_sign && result_sign != minuend_sign;

							valueFlag(F_PV, overflow);
							valueFlag(F_S, (result & 0x8000) != 0);
							valueFlag(F_Z, result == 0);
							valueFlag(F_N, 1);

							setFlagsOther(result >> 8);

							machine->H = (result & 0xff00) >> 8;
							machine->L = result & 0xff;
							machine->pc += 2;
							
						} else if(code[1] == 0x47) {		// LD I,A
							
							printf("LD\tI,A\n");
							
							machine->I = machine->A;
							
							machine->pc += 2;
						
						} else if(code[1] == 0x4b) {		// LD BC,(word)
							
							printf("LD\tBC,$%02x%02x\n",code[3],code[2]);
							
							uint16_t value = read16(machine->pc + 2);
							
							machine->B = (value & 0xff00) >> 8;
							machine->C = value & 0xff;

							machine->pc += 4;
						
						} else if(code[1] == 0x5b) {		//LD DE,(word)
							
							printf("LD\tDE,$%02x%02x\n",code[3],code[2]);

							uint16_t value = read16(machine->pc + 2);
							
							machine->D = (value & 0xff00) >> 8;
							machine->E = value & 0xff;

							machine->pc += 4;

						} else if(code[1] == 0x43) {		// LD (word),BC
							
							uint16_t offset = read16(machine->pc + 2);
							printf("LD $%02x,BC\n",offset);
							uint16_t bc = (machine->B << 8) | (machine->C);
							write16(offset,bc);
							machine->pc += 4;
					
						} else if(code[1] == 0x53) {		// LD (word),DE
							
							uint16_t offset = read16(machine->pc + 2);
							printf("LD $%02x,DE\n",offset);
							uint16_t de = (machine->D << 8) | (machine->E);
							write16(offset,de);
							machine->pc += 4;
						} else if(code[1] == 0xb0) {		// LDIR
						
							printf("LDIR\n");	
							uint16_t hl = (machine->H << 8) | (machine->L);
							uint16_t de = (machine->D << 8) | (machine->E);
							uint16_t bc = (machine->B << 8) | (machine->C);

							uint8_t value = read8(hl);
							write8(de,value);

							de++;
							hl++;
							bc--;

							valueFlag(F_5, (machine->A + value) & 0x02);
							valueFlag(F_3, ((machine->A + value) & F_3) != 0);
							resetFlag(F_H | F_N);
							valueFlag(F_PV, bc != 0);

							machine->D = (de & 0xff00) >> 8;
							machine->E = de & 0xff;
							
							machine->H = (hl & 0xff00) >> 8;
							machine->L = hl & 0xff;

							machine->B = (bc & 0xff00) >> 8;
							machine->C = bc & 0xff;

							if(bc == 0) {
								machine->pc += 2;
							}
							
						} else if (code[1] == 0x5e) { 		// IM 1
							
							printf("IM 1\n");

							machine->IM = 1;

							machine->pc += 2;

						} else {
							printf("16-bit opcode ED%02x\n",code[1]);
							exit(0);
						}
					}
			break;
		case 0xee:	unimplementedInstruction(code); break;
		case 0xef:	unimplementedInstruction(code); break;
		case 0xf0:	unimplementedInstruction(code); break;
		case 0xf1:											// POP AF
					{
						exit(0);
					}
			break;
		case 0xf2:	unimplementedInstruction(code); break;
		case 0xf3:											// DI
					{
						machine->IFF1 = 0;
						machine->IFF2 = 0;
						machine->pc++;
					}
			break;									
		case 0xf4:	unimplementedInstruction(code); break;
		case 0xf5:											 // PUSH AF
					{
						 push((machine->A << 8) | (machine->F));
						 machine->pc++;
					}
			break;
		case 0xf6:	unimplementedInstruction(code); break;
		case 0xf7:	unimplementedInstruction(code); break;
		case 0xf8:	unimplementedInstruction(code); break;
		case 0xf9:	unimplementedInstruction(code); break;
		case 0xfa:											// JP M,address
					{
						if(getFlag(F_S)) {
							machine->pc = (code[2] << 8) | (code[1]);
						} else {
							machine->pc += 3;
						}
					}
				break;
		case 0xfb: 											// EI
					{
						machine->IFF1 = machine->IFF2 = IE_EI;
						machine->defer_int = 1;
						printf("IFF1: %d\n",machine->IFF1);
						machine->pc++;
					}
				break;
		case 0xfc:											// CALL M,address
					{
						if(getFlag(F_S)) {
							uint16_t offset = read16(machine->pc + 1);
							machine->pc += 3;
							push(machine->pc);
							machine->pc = offset;
						} else {
							machine->pc += 3;
						}
					}
				break;
		case 0xfd:	
					{
						if(code[1] == 0x36) { 				// LD (IY+index),byte
							
							printf("LD (IY+%02x),%02x\n",read8(machine->pc + 2),read8(machine->pc + 3));

							uint8_t offset = read8(machine->pc + 2);
							uint8_t value = read8(machine->pc + 3);

							write8(machine->IY + offset, value);

							machine->pc += 4;
						} else {
							printf("16-bit opcode: %02x%02x\n",read8(machine->pc),read8(machine->pc + 1));
						}	
					}
				break;
		case 0xfe:											// CP byte
					{
				
						uint8_t value = code[1];

						setFlag(F_N);
						valueFlag(F_H, (((machine->A & 0x0f) - (value & 0x0f)) & 0x10) != 0);
						
						uint16_t result = machine->A - value;

						valueFlag(F_S, ((result & 0x80) != 0));
						valueFlag(F_C, ((result & 0x100) != 0));
						valueFlag(F_Z, ((result & 0xff) == 0));

						int minuend_sign = machine->A & 0x80;
						int subtrahend_sign = value & 0x80;
						int result_sign = result & 0x80;
						int overflow = minuend_sign != subtrahend_sign && result_sign != minuend_sign;

						valueFlag(F_PV, overflow);
						setFlagsOther(result);

						machine->pc += 2;
					}
				break;
		case 0xff:	unimplementedInstruction(code); break;
	}

	printf("\t");
	printf("%c", getFlag(F_Z) ? 'Z' : '.');
	printf("%c", getFlag(F_S) ? 'S' : '.');
	printf("%s", getFlag(F_PV) ? "PV" : ".");
	printf("%c", getFlag(F_C) ? 'C' : '.');
	printf("%c", getFlag(F_N) ? 'N' : '.');
	printf("%c", getFlag(F_3) ? '3' : '.');
	printf("%c", getFlag(F_5) ? '5' : '.');
	printf("%c  ", getFlag(F_H) ? 'H' : '.');

	printf("\tA $%02x B $%02x C $%02x D $%02x E $%02x F $%02x H $%02x L $%02x SP $%02x I $%02x IX $%02x\n", machine->A, machine->B, machine->C,
				           machine->D, machine->E, machine->F, machine->H, machine->L, machine->sp, machine->I, machine->IX);	
}






